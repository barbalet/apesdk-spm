import XCTest
@testable import ApeSDKSPM

final class ApeSDKSPMTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(ApeSDKSPM().text, "Hello, World!")
    }
}
